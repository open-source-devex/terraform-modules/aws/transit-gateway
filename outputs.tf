output "transit_gateway_id" {
  value = join("", aws_ec2_transit_gateway.this.*.id)
}

output "transit_gateway_arn" {
  value = join("", aws_ec2_transit_gateway.this.*.arn)
}

output "ram_transit_gateway_arn" {
  value = join("", aws_ram_resource_share.this.*.arn)
}
