variable "enabled" {
  description = "Whether to create the resources defined in the module"
  type        = bool
  default     = true
}

##
# Metadata
##
variable "name" {
  description = "Name of the VPN attachment"
}

variable "global_tags" {
  description = "Tags appended to all resources that will take them"
  type        = map(string)
}

##
# Transit Gateway related settings
##
variable "transit_gateway_id" {
  description = "Transit gateway to attach to"
  default     = ""
}

variable "additional_routes" {
  description = "Additional routes pushed in the route table"

  type = list(object({
    tga_id : string,
    route : string
  }))
}

variable "transit_gateway_route_tables" {
  description = "List of Transit Gateway Routable ID's to which the VPN static routes should be pushed"
  type        = list(string)
  default     = []
}

##
# VPN Connection related settings
##
variable "customer_gateway_ip" {
  description = "IP address of the remote endpoint"
  type        = "string"

}

variable "customer_gateway_bgp_asn" {
  description = "BGP ASN for the customer gateway"
  type        = "string"
}

variable "vpn_connection_static_routes_only" {
  description = "Set to true for the created VPN connection to use static routes exclusively (only if `create_vpn_connection = true`). Static routes must be used for devices that don't support BGP."
  type        = bool
  default     = false
}

variable "vpn_connection_static_routes" {
  description = "List of CIDRs to be used as destination for static routes"
  type        = list(string)
  default     = []
}
