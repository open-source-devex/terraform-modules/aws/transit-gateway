locals {
  vpn_transit_gateway_attachment_id = module.vpn_connection.vpn_connection_transit_gateway_attachment_id
  routes_to_push                    = setproduct(var.transit_gateway_route_tables, var.vpn_connection_static_routes)
  transit_gateway_route_table_id    = join("", aws_ec2_transit_gateway_route_table.this.*.id)
}

module "vpn_connection" {
  source  = "terraform-aws-modules/vpn-gateway/aws"
  version = "~> 2.5"

  create_vpn_connection = var.enabled

  transit_gateway_id            = var.transit_gateway_id
  connect_to_transit_gateway    = true
  customer_gateway_id           = join("", aws_customer_gateway.this.*.id)
  create_vpn_gateway_attachment = false

  vpn_connection_static_routes_only = var.vpn_connection_static_routes_only

  tags = merge(var.global_tags, {
    Name = "${var.name}-vpn"
  })
}

resource "aws_customer_gateway" "this" {
  count = var.enabled ? 1 : 0

  bgp_asn    = var.customer_gateway_bgp_asn
  ip_address = var.customer_gateway_ip
  type       = "ipsec.1"

  tags = merge(var.global_tags, {
    Name = "${var.name}-cgw"
  })
}

resource "aws_ec2_transit_gateway_route_table" "this" {
  count = var.enabled ? 1 : 0

  transit_gateway_id = var.transit_gateway_id

  tags = merge(var.global_tags, {
    Name = "${var.name}-rt"
  })
}

resource "aws_ec2_transit_gateway_route_table_association" "this" {
  count = var.enabled ? 1 : 0

  transit_gateway_attachment_id  = local.vpn_transit_gateway_attachment_id
  transit_gateway_route_table_id = local.transit_gateway_route_table_id
}

resource "aws_ec2_transit_gateway_route" "vpn_connection_static_routes" {
  for_each = toset(var.enabled ? var.vpn_connection_static_routes : [])

  transit_gateway_route_table_id = local.transit_gateway_route_table_id
  transit_gateway_attachment_id  = local.vpn_transit_gateway_attachment_id

  destination_cidr_block = each.value
}

resource "aws_ec2_transit_gateway_route" "additional_routes" {
  for_each = var.enabled ? { for route in var.additional_routes : "${route.tga_id}-${route.route}" => route} : {}

  transit_gateway_route_table_id = local.transit_gateway_route_table_id
  transit_gateway_attachment_id  = each.value.tga_id
  destination_cidr_block         = each.value.route
}

resource "aws_ec2_transit_gateway_route" "push_routes_into_tgw_rt" {
  for_each = var.enabled ? { for route in local.routes_to_push : "${route[0]}-${route[1]}" => route} : {}

  transit_gateway_route_table_id = each.value[0]
  transit_gateway_attachment_id  = local.vpn_transit_gateway_attachment_id
  destination_cidr_block         = each.value[1]
}
