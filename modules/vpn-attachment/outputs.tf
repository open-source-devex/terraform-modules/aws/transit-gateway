output "vpn_transit_gateway_attachment_id" {
  value = module.vpn_connection.vpn_connection_transit_gateway_attachment_id
}

output "vpn_transit_gateway_route_table_id" {
  value = local.transit_gateway_route_table_id
}
