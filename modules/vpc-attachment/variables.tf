variable "enabled" {
  description = "Whether to create the resources defined in the module"
  type        = bool
  default     = true
}

variable "tgw_id" {
  description = "Transit gateway to attach to"
  default     = ""
}

variable "vpc_id" {
  description = "VPC ID to attach to the gateway"
  default     = ""
}

variable "subnet_ids" {
  description = "Subnets to expose via the gateway"
  type        = list(string)
  default     = [""]
}

variable "create_new_tgw_rt" {
  description = "Create a new transit gateway route table"
  default     = false
}

variable "existing_tgw_rt" {
  description = "Store routes in existing transit gateway route table"
  default     = ""
}

variable "tgw_default_route_table_association" {
  description = "Whether resource attachments are automatically associated with the default association route table"
  default     = false
}

variable "tgw_default_route_table_propagation" {
  description = "Whether resource attachments automatically propagate routes to the default propagation route table"
  default     = false
}
