resource "aws_ec2_transit_gateway_vpc_attachment" "this" {
  count = var.enabled ? 1 : 0

  subnet_ids         = var.subnet_ids
  transit_gateway_id = var.tgw_id
  vpc_id             = var.vpc_id

  transit_gateway_default_route_table_association = var.tgw_default_route_table_association
  transit_gateway_default_route_table_propagation = var.tgw_default_route_table_propagation
}

resource "aws_ec2_transit_gateway_route_table" "route_table" {
  count = var.enabled && var.create_new_tgw_rt ? 1 : 0

  transit_gateway_id = var.tgw_id
}
