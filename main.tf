resource "aws_ec2_transit_gateway" "this" {
  count = var.enabled && var.create_tgw ? 1 : 0

  description = var.tgw_gateway_description

  amazon_side_asn                 = var.tgw_amazon_side_asn
  auto_accept_shared_attachments  = var.tgw_auto_accept_shared_attachments
  default_route_table_association = var.tgw_default_route_table_association
  default_route_table_propagation = var.tgw_default_route_table_propagation
  dns_support                     = var.tgw_dns_support

  vpn_ecmp_support = var.tgw_vpn_ecmp_support
  tags             = var.tgw_tags
}

resource "aws_ram_resource_share" "this" {
  count = var.enabled && var.share_tgw ? 1 : 0

  name                      = var.ram_share_name
  allow_external_principals = false
  tags                      = var.ram_share_tags
}

resource "aws_ram_resource_association" "this" {
  count = var.enabled && var.create_tgw && var.share_tgw ? 1 : 0

  resource_arn       = join("", aws_ec2_transit_gateway.this.*.arn)
  resource_share_arn = join("", aws_ram_resource_share.this.*.arn)
}

resource "aws_ram_principal_association" "this" {
  count = var.enabled && var.share_tgw_with != "" ? 1 : 0

  principal          = var.share_tgw_with
  resource_share_arn = join("", aws_ram_resource_share.this.*.arn)
}
