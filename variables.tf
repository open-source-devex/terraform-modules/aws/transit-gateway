variable "enabled" {
  description = "Whether to create the resources defined in the module"
  type        = bool
  default     = true
}

variable "tgw_amazon_side_asn" {
  description = "ASN of AWS Side of a BGP Session"
  default     = "64512"
}

variable "tgw_auto_accept_shared_attachments" {
  description = "Whether resource requests are automatically accepted"
  default     = "disable"
}

variable "tgw_default_route_table_association" {
  description = "Whether resource attachments are automatically associated with the default association route table"
  default     = "disable"
}

variable "tgw_default_route_table_propagation" {
  description = "Whether resource attachments automatically propagate routes to the default propagation route table"
  default     = "disable"
}

variable "tgw_gateway_description" {
  description = "Description of the Gateway"
  default     = ""
}

variable "create_tgw" {
  description = "Create a transit gateway or not"
  default     = true
}

variable "share_tgw" {
  description = "Share the Gateway using AWS RAM resource"
  default     = true
}

variable "tgw_dns_support" {
  description = "Whether DNS support is enabled"
  default     = "enable"
}

variable "tgw_tags" {
  description = "Key Value Tags for the EC2 Transit Gateway"
  default     = {}

  type = map
}

variable "tgw_vpn_ecmp_support" {
  description = "Whether VPN Equal Cost Multipath protocol is enabled"
  default     = "disable"
}

variable "vpc_id" {
  description = "VPC ID to attach gateway to"
  default     = ""
}

variable "default_tags" {
  description = "Default Tags which are merged into resource tags"
  default     = {}

  type = map
}

variable "ram_share_tags" {
  description = "Tags for RAM Resource Share"
  default     = {}

  type = map
}

variable "ram_share_name" {
  description = "Name of the RAM Resource Share"
  default     = ""
}

variable "share_tgw_with" {
  description = "ARN of principal to share this TGW with"
  default     = ""
}
